import { TextField, ThemeProvider } from "@mui/material"
import theme from "./themes/theme"

function App() {
  return (
   <ThemeProvider theme={theme}>
     <TextField label='userName'/>
   </ThemeProvider>
  )
}

export default App
